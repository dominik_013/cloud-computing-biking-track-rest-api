package at.fhhagenberg.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;
import org.springframework.data.redis.core.RedisHash;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@RedisHash("BikingTrack")
public class BikingTrack implements Serializable {

    @Id
    private String id;

    @NotEmpty(message = "Name has to be specified and should not be empty")
    private String name;

    private boolean isFinalized;

    @JsonManagedReference
    @Reference
    private List<Coordinate> coordinateList;

    public BikingTrack() {

    }

    public BikingTrack(BikingTrack track) {
        this.id = track.id;
        this.name = track.name;
        this.isFinalized = track.isFinalized;
        this.coordinateList = track.coordinateList;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isFinalized() {
        return isFinalized;
    }

    public void setFinalized(boolean finalized) {
        isFinalized = finalized;
    }

    public List<Coordinate> getCoordinateList() {
        return coordinateList;
    }

    public void setCoordinateList(List<Coordinate> coordinateList) {
        this.coordinateList = coordinateList;
    }
}
