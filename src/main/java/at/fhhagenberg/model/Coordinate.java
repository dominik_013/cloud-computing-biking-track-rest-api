package at.fhhagenberg.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;
import org.springframework.data.redis.core.RedisHash;

import javax.validation.constraints.NotNull;

@RedisHash("Coordinate")
public class Coordinate {

    @Id
    private String coordinateId;

    @NotNull
    private double longitude;

    @NotNull
    private double latitude;

    @JsonBackReference
    @Reference
    private BikingTrack bikingTrack;


    public Coordinate() {

    }

    public Coordinate(Coordinate coordinate) {
        this.longitude = coordinate.longitude;
        this.latitude = coordinate.latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setBikingTrack(BikingTrack bikingTrack) {
        this.bikingTrack = bikingTrack;
    }

    public BikingTrack getBikingTrack() {
        return bikingTrack;
    }
}
