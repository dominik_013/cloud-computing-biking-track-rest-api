package at.fhhagenberg.service;

import at.fhhagenberg.exceptions.InvalidRequestBodyException;
import at.fhhagenberg.exceptions.ResourceNotFoundException;
import at.fhhagenberg.model.BikingTrack;
import at.fhhagenberg.model.Coordinate;
import at.fhhagenberg.repository.BikingTrackRepository;
import at.fhhagenberg.repository.CoordinateRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.Unirest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class BikingTrackServiceBean implements BikingTrackService {

    private final BikingTrackRepository bikingTrackRepository;
    private final CoordinateRepository coordinateRepository;

    @Autowired
    public BikingTrackServiceBean(BikingTrackRepository bikingTrackRepository, CoordinateRepository coordinateRepository) {
        this.bikingTrackRepository = bikingTrackRepository;
        this.coordinateRepository = coordinateRepository;
    }

    @Override
    public BikingTrack create(BikingTrack track) {

        // Ensure the entity object to be created does NOT exist in the
        // repository. Prevent the default behavior of save() which will update
        // an existing entity if the entity matching the supplied id exists.
        if (track.getId() != null) {
            // Cannot create DiaryEntry with specified ID value
            throw new InvalidRequestBodyException("The id attribute must be null to persist.");
        }

        // Throw exception if track with name already exists
        for (BikingTrack t : bikingTrackRepository.findAll()) {
            if (t.getName().equals(track.getName())) {
                throw new InvalidRequestBodyException("The biking track with the name " + track.getName() + " already exists, use a different name");
            }
        }

        // Finalized always should be false when creating new tracks
        track.setFinalized(false);

        return bikingTrackRepository.save(track);
    }

    @Override
    public Collection<BikingTrack> getAllBikingTracks() {
        // This is dirty - not enough time, sry
        Collection<BikingTrack> collection = new ArrayList<>();
        Iterable<BikingTrack> it = bikingTrackRepository.findAll();
        for (BikingTrack track : it) {
            Iterable<Coordinate> itCoords = coordinateRepository.findAll();
            List<Coordinate> coordsToAdd = new ArrayList<>();
            for (Coordinate coordinate : itCoords) {
                if (coordinate.getBikingTrack().getId().equals(track.getId())) {
                    coordsToAdd.add(coordinate);
                }
            }
            track.setCoordinateList(coordsToAdd);
        }
        it.forEach(collection::add);
        return collection;
    }

    @Override
    public BikingTrack getBikingTrack(String trackId) {
        // This is dirty - not enough time, sry
        Optional<BikingTrack> trackOptional = bikingTrackRepository.findById(trackId);
        if (trackOptional.isPresent()) {
            BikingTrack track = trackOptional.get();

            Iterable<Coordinate> itCoords = coordinateRepository.findAll();
            List<Coordinate> coordsToAdd = new ArrayList<>();
            for (Coordinate coordinate : itCoords) {
                if (coordinate.getBikingTrack().getId().equals(track.getId())) {
                    coordsToAdd.add(coordinate);
                }
            }
            track.setCoordinateList(coordsToAdd);

            return track;
        }

        throw new ResourceNotFoundException("BikingTrack with trackId " + trackId + " not found");
    }

    @Override
    public BikingTrack finalizeBikingTrack(String trackId) {
        return bikingTrackRepository.findById(trackId)
                .map(track -> {
                    track.setFinalized(true);

                    bikingTrackRepository.save(track);

                    Iterable<Coordinate> itCoords = coordinateRepository.findAll();
                    List<Coordinate> coordsToAdd = new ArrayList<>();
                    for (Coordinate coordinate : itCoords) {
                        if (coordinate.getBikingTrack().getId().equals(track.getId())) {
                            coordsToAdd.add(coordinate);
                        }
                    }
                    track.setCoordinateList(coordsToAdd);

                    return track;
                }).orElseThrow(() -> new ResourceNotFoundException("BikingTrack with trackId " + trackId + " not found"));
    }

    @Override
    public Coordinate addCoordinateToBikingTrack(Coordinate coordinate, String trackId) {
        if (coordinate.getBikingTrack() != null) {
            // Cannot create DiaryEntry with specified ID value
            throw new InvalidRequestBodyException("The BikingTrack the coordinate belongs to should not be posted in the request body," +
                    "only post the trackId within the URL");
        }

        Optional<BikingTrack> res = bikingTrackRepository.findById(trackId);
        if (res.isPresent() && res.get().isFinalized()) {
            throw new InvalidRequestBodyException("BikingTrack with trackId " + trackId + " is already finalized, can't add additional coordinates");
        }

        return bikingTrackRepository.findById(trackId)
                .map(track -> {
                    coordinate.setBikingTrack(track);
                    return coordinateRepository.save(coordinate);
                }).orElseThrow(() -> new ResourceNotFoundException("BikingTrack with trackId " + trackId + " not found"));
    }

    @Override
    public Collection<BikingTrack> emailBikingTracksToAdmin() {
        Collection<BikingTrack> tracks = getAllBikingTracks();

        // Send email
        Gson gson = new Gson();
        Unirest.post("https://api.mailgun.net/v3/sandboxf76cd45b5d6841508747e31273983841.mailgun.org/messages")
                .field("from", "Biking Track Server <mailgun@sandboxf76cd45b5d6841508747e31273983841.mailgun.org>")
                .field("to", "d.reichinger95@gmail.com")
                .field("subject", "Biking Tracks")
                .field("text", gson.toJson(tracks))
                .basicAuth("api", "aa27785153d2774dcfeddd17ea421ce7-b3780ee5-3257be2f")
                .asJsonAsync();


        return tracks;
    }
}
