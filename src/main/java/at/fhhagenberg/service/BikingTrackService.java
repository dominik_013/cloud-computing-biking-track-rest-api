package at.fhhagenberg.service;

import at.fhhagenberg.model.BikingTrack;
import at.fhhagenberg.model.Coordinate;

import java.util.Collection;

public interface BikingTrackService {

    BikingTrack create(BikingTrack track);

    Collection<BikingTrack> getAllBikingTracks();

    BikingTrack getBikingTrack(String trackId);

    BikingTrack finalizeBikingTrack(String trackId);

    Coordinate addCoordinateToBikingTrack(Coordinate coordinate, String trackId);

    Collection<BikingTrack> emailBikingTracksToAdmin();

}
