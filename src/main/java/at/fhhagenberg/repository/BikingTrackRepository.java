package at.fhhagenberg.repository;

import at.fhhagenberg.model.BikingTrack;
import org.springframework.data.repository.CrudRepository;

public interface BikingTrackRepository extends CrudRepository<BikingTrack, String> {

    BikingTrack findBikingTrackByName(String name);

}