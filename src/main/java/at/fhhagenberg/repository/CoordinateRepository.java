package at.fhhagenberg.repository;

import at.fhhagenberg.model.Coordinate;
import org.springframework.data.repository.CrudRepository;

public interface CoordinateRepository extends CrudRepository<Coordinate, String> {

}
